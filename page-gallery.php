<?php
/**
 * Template Name: Gallery
 */

get_header();
?>

<div class="wrap">

    <div id="app">
        <div>
            <div class="promo-all">
                <div class="promo-img gallery-img" style="background:url(https://d3l3qn7kx5ewr2.cloudfront.net/wp-content/uploads/20190409122841/fon.jpg) repeat;">
                    <div class="promo-all-cont container">
                        <h1 class="wow bounceInDown" data-wow-duration="2s" style="visibility: visible; animation-duration: 2s; animation-name: bounceInDown;"><?php the_title(); ?></h1>
                    </div>
                </div>
                <div class="container"></div>
            </div>
            <div class="gallery">
                <div class="gallery-item wow fadeInUp" data-wow-duration="1s" style="visibility: visible; animation-duration: 1s; animation-name: fadeInUp;">
                    <a href="https://d3l3qn7kx5ewr2.cloudfront.net/wp-content/uploads/20190409122840/fasady-domov-foto-odnoyetazhnykh-domov-1.jpg" data-lightbox="images">
                        <img src="https://d3l3qn7kx5ewr2.cloudfront.net/wp-content/uploads/20190409122840/fasady-domov-foto-odnoyetazhnykh-domov-1.jpg" alt="fasady-domov-foto-odnoyetazhnykh-domov-1.jpg">
                    </a>
                </div>
                <div class="gallery-item wow fadeInUp" data-wow-duration="1s" style="visibility: visible; animation-duration: 1s; animation-name: fadeInUp;">
                    <a href="https://d3l3qn7kx5ewr2.cloudfront.net/wp-content/uploads/20190409122839/fasaddomaotdelanklinkernymkirpichom.jpg" data-lightbox="images"">
                        <img src="https://d3l3qn7kx5ewr2.cloudfront.net/wp-content/uploads/20190409122839/fasaddomaotdelanklinkernymkirpichom.jpg" alt="fasaddomaotdelanklinkernymkirpichom.jpg">
                    </a>
                </div>
                <div class="gallery-item wow fadeInUp" data-wow-duration="1s" style="visibility: visible; animation-duration: 1s; animation-name: fadeInUp;">
                    <a href="https://d3l3qn7kx5ewr2.cloudfront.net/wp-content/uploads/20190411234805/170220_8_No.jpg" data-lightbox="images">
                        <img src="https://d3l3qn7kx5ewr2.cloudfront.net/wp-content/uploads/20190411234805/170220_8_No.jpg" alt="170220_8_No.jpg">
                    </a>
                </div>
                <div class="gallery-item wow fadeInUp" data-wow-duration="1s" style="visibility: visible; animation-duration: 1s; animation-name: fadeInUp;">
                    <a href="https://d3l3qn7kx5ewr2.cloudfront.net/wp-content/uploads/20190409122927/profnastil1-4.jpg" data-lightbox="images">
                        <img src="https://d3l3qn7kx5ewr2.cloudfront.net/wp-content/uploads/20190409122927/profnastil1-4.jpg" alt="profnastil1-4.jpg">
                    </a>
                </div>
                <div class="gallery-item wow fadeInUp" data-wow-duration="1s" style="visibility: visible; animation-duration: 1s; animation-name: fadeInUp;">
                    <a href="https://d3l3qn7kx5ewr2.cloudfront.net/wp-content/uploads/20190409122839/fasad-proflist.jpg" data-lightbox="images">
                        <img src="https://d3l3qn7kx5ewr2.cloudfront.net/wp-content/uploads/20190409122839/fasad-proflist.jpg" alt="fasad-proflist.jpg">
                    </a>
                </div>
                <div class="gallery-item wow fadeInUp" data-wow-duration="1s" style="visibility: visible; animation-duration: 1s; animation-name: fadeInUp;">
                    <a href="https://d3l3qn7kx5ewr2.cloudfront.net/wp-content/uploads/20190409122857/klinkernyykirpichsmotritsyanafasadeochenprivlekatelno.jpg" data-lightbox="images">
                        <img src="https://d3l3qn7kx5ewr2.cloudfront.net/wp-content/uploads/20190409122857/klinkernyykirpichsmotritsyanafasadeochenprivlekatelno.jpg" alt="klinkernyykirpichsmotritsyanafasadeochenprivlekatelno.jpg">
                    </a>
                </div>
                <div class="gallery-item wow fadeInUp" data-wow-duration="1s" style="visibility: visible; animation-duration: 1s; animation-name: fadeInUp;">
                    <a href="https://d3l3qn7kx5ewr2.cloudfront.net/wp-content/uploads/20190409122853/kak-vybrat-fasadnyj-proflist.jpg" data-lightbox="images">
                        <img src="https://d3l3qn7kx5ewr2.cloudfront.net/wp-content/uploads/20190409122853/kak-vybrat-fasadnyj-proflist.jpg" alt="kak-vybrat-fasadnyj-proflist.jpg">
                    </a>
                </div>
                <div class="gallery-item wow fadeInUp" data-wow-duration="1s" style="visibility: visible; animation-duration: 1s; animation-name: fadeInUp;">
                    <a href="https://d3l3qn7kx5ewr2.cloudfront.net/wp-content/uploads/20190409122931/R-panel-metal-roof-exposed-fastener.jpg" data-lightbox="images">
                        <img src="https://d3l3qn7kx5ewr2.cloudfront.net/wp-content/uploads/20190409122931/R-panel-metal-roof-exposed-fastener.jpg" alt="R-panel-metal-roof-exposed-fastener.jpg">
                    </a>
                </div>
                <div class="gallery-item wow fadeInUp" data-wow-duration="1s" style="visibility: visible; animation-duration: 1s; animation-name: fadeInUp;">
                    <a href="https://d3l3qn7kx5ewr2.cloudfront.net/wp-content/uploads/20190409122928/profnastil-dlya-krishi-sten-fasda-e1532031491954.jpg" data-lightbox="images">
                        <img src="https://d3l3qn7kx5ewr2.cloudfront.net/wp-content/uploads/20190409122928/profnastil-dlya-krishi-sten-fasda-e1532031491954.jpg" alt="profnastil-dlya-krishi-sten-fasda-e1532031491954.jpg">
                    </a>
                </div>
                <div class="gallery-item wow fadeInUp" data-wow-duration="1s" style="visibility: visible; animation-duration: 1s; animation-name: fadeInUp;">
                    <a href="https://d3l3qn7kx5ewr2.cloudfront.net/wp-content/uploads/20190409122833/detal_s20-5.jpg" data-lightbox="images">
                        <img src="https://d3l3qn7kx5ewr2.cloudfront.net/wp-content/uploads/20190409122833/detal_s20-5.jpg" alt="detal_s20-5.jpg">
                    </a>
                </div>
                <div class="gallery-item wow fadeInUp" data-wow-duration="1s" style="visibility: visible; animation-duration: 1s; animation-name: fadeInUp;">
                    <a href="https://d3l3qn7kx5ewr2.cloudfront.net/wp-content/uploads/20190411235051/1355731301_gf2.jpg" data-lightbox="images">
                        <img src="https://d3l3qn7kx5ewr2.cloudfront.net/wp-content/uploads/20190411235051/1355731301_gf2.jpg" alt="1355731301_gf2.jpg">
                    </a>
                </div>
                <div class="gallery-item wow fadeInUp" data-wow-duration="1s" style="visibility: visible; animation-duration: 1s; animation-name: fadeInUp;">
                    <a href="https://d3l3qn7kx5ewr2.cloudfront.net/wp-content/uploads/20190411235135/02554594_n4.jpg" data-lightbox="images">
                        <img src="https://d3l3qn7kx5ewr2.cloudfront.net/wp-content/uploads/20190411235135/02554594_n4.jpg" alt="02554594_n4.jpg">
                    </a>
                </div>
            </div>
        </div>
    </div>

</div>


<?php
get_footer();
