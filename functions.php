<?php

if ( ! function_exists( 'twentynineteen_the_posts_navigation' ) ) :
    /**
     * Documentation for function.
     */
    function twentynineteen_the_posts_navigation() {
        the_posts_pagination(
            array(
                'mid_size'  => 2,
                'prev_text' => sprintf(
                    '%s <span class="nav-prev-text">%s</span>',
                    "<",
                    __( 'Newer posts', 'twentynineteen' )
                ),
                'next_text' => sprintf(
                    '<span class="nav-next-text">%s</span> %s',
                    __( 'Older posts', 'twentynineteen' ),
                    ">"
                ),
            )
        );
    }
endif;

function asgard_enqueue_scripts(){
    wp_enqueue_script( "slick-min-js", get_template_directory_uri() . "/js/slick/slick.min.js", array('jquery'), false, true );
    wp_enqueue_script( "slick-js", get_template_directory_uri() . "/js/slick.js", array('jquery'), false, true );
    wp_enqueue_script( "base-js", get_template_directory_uri() . "/js/base.js", array('jquery'), false, true );
    wp_enqueue_script( "wow-min-js", get_template_directory_uri() . "/js/wow.min.js", array('jquery'), false, true );
    wp_enqueue_script( "jquery-textillate-js", get_template_directory_uri() . "/js/jquery.textillate.js", array('jquery'), false, true );
    wp_enqueue_script( "jquery-lettering-js", get_template_directory_uri() . "/js/jquery.lettering.js", array('jquery'), false, true );
    wp_enqueue_script( "tlt-js", get_template_directory_uri() . "/js/tlt.js", array('jquery'), false, true );
    wp_enqueue_script( "lightbox-js", get_template_directory_uri() . "/js/lightbox.js", array('jquery'), false, true );
    wp_enqueue_script( "custom-js", get_template_directory_uri() . "/js/custom.js", array('jquery'), false, true );

    wp_localize_script('base-js', 'admin_ajax',
        array(
            'url' => admin_url('admin-ajax.php')
        )
    );
}
add_action('wp_enqueue_scripts', 'asgard_enqueue_scripts');

function asgard_enqueue_styles(){
    wp_enqueue_style( "core-css", get_template_directory_uri() . "/css/core.css" );
}
add_action('wp_print_styles', 'asgard_enqueue_styles');

function cc_mime_types($mimes) {
    $mimes['svg'] = 'image/svg+xml';
    return $mimes;
}
add_filter('upload_mimes', 'cc_mime_types');

function sendMailAjax(){
    echo sendMail();

    wp_die();
}

function sendMail() {
    $name  = $_POST['name'];
    $phone = $_POST['phone'];
    $date  = date('d.m.Y - H:i:s');
    $to    = 'asgard.ksm@gmail.com';
    $from  = 'asgard@gmail.com';

    $subject = "Обратный звонок с сайта `Asgard`".$name." ".$phone;
    $subject = "=?utf-8?B?".base64_encode($subject)."?=";
    $headers = "From: $from\r\nReply-to: $from\r\nContent-type: text/html; charset=utf-8\r\n";

    $message = "
    <div style='border-radius: 3px; background: #333; margin: 2px; padding: 15px; box-sizing: border-box;'>
     <b style='color: #eee'>Заявка на консультацию - $date</b>
     </div>
     <br>

     <div style='border-radius: 3px; background: #ccc; margin: 2px; padding: 15px; box-sizing: border-box;'>
     ИНФОРМАЦИЯ О ЗАКАЗЧИКЕ:
     </div>
     <div style='border-radius: 3px; background: #eee; margin: 2px; padding: 10px; box-sizing: border-box;'> ФИО: $name
     </div>
      <div style='border-radius: 3px; background: #eee; margin: 2px; padding: 10px; box-sizing: border-box;'> Телефон: $phone
     </div>
                ";

    wp_mail( $to, $subject, $message, $headers );

    return true;
}

add_action("wp_ajax_callrequest", "sendMailAjax");
add_action("wp_ajax_nopriv_callrequest", "sendMailAjax");

function font_awesome() {
    if ( !is_admin() ) {
        wp_register_style('font-awesome', 'https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.6.3/css/font-awesome.min.css');
        wp_enqueue_style('font-awesome');
    }
}
add_action('wp_enqueue_scripts', 'font_awesome');

function request_call(){
    $respnse = ["success" => false];

    if ( empty( $_POST['phone'] ) || strlen( $_POST['phone'] ) < 10 ) {
        $respnse["message"] = "Введіть існуючий номер мобільного. Зразок: 0667778855";
        echo json_encode( $respnse );
        wp_die();
    }

    if ( sendMail() ) {
        $respnse = ["success" => true];
    } else {
        $respnse["message"] = "Виникла помилка при надсиланні повідомлення. Спробуйте пізніше.";
    }

    echo json_encode( $respnse );
    wp_die();
}
add_action("wp_ajax_request_call", "request_call");
add_action("wp_ajax_nopriv_request_call", "request_call");
