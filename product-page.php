<?php
/**
 * Template Name: Product Page
 */

get_header();

$attc   = get_field( "pd_main_image" );
$groups = get_field( "ppd_groups" );
$backgr = "";

if ( !empty( $attc ) && !empty( $attc["url"] )  ) {
    $backgr = "background:url(".$attc["url"].") no-repeat center/cover;";
}

?>

<div class="wrap">

    <div id="app">
        <div>
            <div class="promo-all">
                <div class="promo-img" style="<?php echo $backgr; ?>">
                    <div class="promo-all-cont container">
                        <h1 class="wow bounceInDown" data-wow-duration="2s" style="visibility: visible; animation-duration: 2s; animation-name: bounceInDown;"><?php the_title(); ?></h1>
                        <p><?php echo get_the_content(); ?></p>
                    </div>
                </div>
                <div class="container"></div>
            </div>
            <div class="decor">
                <div class="container">
                    <?php
                        if ( is_array( $groups ) ) :
                            foreach ( $groups as $group ):
                    ?>

                    <div class="decor_elem">
                        <?php
                            if ( $group["ppd_group_type"] == "products" ) {
                                echo "<h2>" . $group["ppd_group_title"] . "</h2>";
                            }
                        ?>
                        <div class="flex">
                            <?php if ( $group["ppd_group_type"] == "products" ) { ?>
                            <?php foreach ( $group["ppd_products"] as $product ): ?>

                                <div class="decor-wrap wow fadeInUp" data-wow-duration="2s" style="visibility: visible; animation-duration: 2s; animation-name: fadeInUp;">
                                    <div class="decor-img">
                                        <img data-wow-duration="2s" style="visibility: visible; animation-duration: 2s; animation-name: pulse;" src="<?php echo $product["ppd_product_image"]; ?>" alt="<?php echo $product["ppd_product_image_alt"]; ?>">
                                        <div class="decor-about"></div>
                                    </div>
                                    <div class="decor-cont">
                                        <h3 class="wow"><?php echo $product["ppd_product_text"]; ?></h3>
                                    </div>
                                </div>

                            <?php endforeach; ?>
                            <?php } else if ( $group["ppd_group_type"] == "text" ) { ?>
                                <div class="decor-wrap text-wrap wow" style="visibility: visible; animation-duration: 2s; animation-name: fadeInUp;">
                                    <?php echo $group["pdd_group_text"]; ?>
                                </div>
                            <?php } ?>
                        </div>
                        <?php
                            if ( $group["ppd_group_type"] == "products" ) {
                                if ( !empty( $group["ppd_color_palette"] ) ) {
                                    get_template_part('template-parts/page/palette', '');
                                } else {
                                    get_template_part('template-parts/page/after-products', '');
                                }
                            }
                        ?>
                    </div>

                    <?php
                            endforeach;
                        endif;
                    ?>

                </div>
            </div>
        </div>
    </div>

</div>


<?php
get_footer();
