<?php
/**
 * Front page template file
 */

get_header();
$old_template = get_field("fp_old_template" );

if ( !empty( $old_template ) && $old_template[0] == "Use old template" ) :
?>

    <div class="wrap">
        <div id="app">
            <div>
                <div class="promo">
                    <div class="slider">
                        <span class="slider_section" style="background: url('https://d3l3qn7kx5ewr2.cloudfront.net/wp-content/uploads/20190409123002/slide1-min.jpg'); width: 1358px; position: relative; left: 0px; top: 0px; z-index: 998; opacity: 0; transition: opacity 2000ms linear 0s;" aria-hidden="true" tabindex="-1" role="option" aria-describedby="slick-slide00"></span>
                        <span class="slider_section" style="background: url('https://d3l3qn7kx5ewr2.cloudfront.net/wp-content/uploads/20190409122957/sl3.jpg'); width: 1358px; position: relative; left: -1358px; top: 0px; z-index: 998; opacity: 0; transition: opacity 2000ms linear 0s;" aria-hidden="true" tabindex="-1" role="option" aria-describedby="slick-slide01"></span>
                        <span class="slider_section" style="background: url('https://d3l3qn7kx5ewr2.cloudfront.net/wp-content/uploads/20190409122947/s8.jpg'); width: 1358px; position: relative; left: -2716px; top: 0px; z-index: 999; opacity: 1;" aria-hidden="false" tabindex="-1" role="option" aria-describedby="slick-slide02"></span>
                        <span class="slider_section" style="background: url('https://d3l3qn7kx5ewr2.cloudfront.net/wp-content/uploads/20190409122958/sl4-min.jpg'); width: 1358px; position: relative; left: -4074px; top: 0px; z-index: 998; opacity: 0; transition: opacity 2000ms linear 0s;" aria-hidden="true" tabindex="-1" role="option" aria-describedby="slick-slide03"></span>
                        <span class="slider_section" style="background: url('https://d3l3qn7kx5ewr2.cloudfront.net/wp-content/uploads/20190409122959/sl5-min.jpg'); width: 1358px; position: relative; left: -5432px; top: 0px; z-index: 998; opacity: 0; transition: opacity 2000ms linear 0s;" aria-hidden="true" tabindex="-1" role="option" aria-describedby="slick-slide04"></span>
                        <span class="slider_section" style="background: url('https://d3l3qn7kx5ewr2.cloudfront.net/wp-content/uploads/20190409122956/sl1-min.jpg'); width: 1358px; position: relative; left: -6790px; top: 0px; z-index: 998; opacity: 0; transition: opacity 2000ms linear 0s;" aria-hidden="true" tabindex="-1" role="option" aria-describedby="slick-slide05"></span>
                        <span class="slider_section" style="background: url('https://d3l3qn7kx5ewr2.cloudfront.net/wp-content/uploads/20190409122957/sl2-min.jpg'); width: 1358px; position: relative; left: -8148px; top: 0px; z-index: 998; opacity: 0; transition: opacity 2000ms linear 0s;" aria-hidden="true" tabindex="-1" role="option" aria-describedby="slick-slide06"></span>
                    </div>
                    <div class="promo-content">
                        <h1>
                            <svg class="intro go">
                                <text text-anchor="middle" x="50%" y="50%" class="text-main text-stroke" clip-path="url(#text1)">АСГАРД</text>
                                <defs>
                                    <clipPath id="text1">
                                        <text text-anchor="middle" x="50%" y="50%" class="text-main">АСГАРД</text>
                                    </clipPath>
                                </defs>
                            </svg>
                        </h1>
                        <h2 class="tlt2" style="">
                        <span aria-label="Лучшее для лучших" style="visibility: hidden;">
                            <span class="word1" aria-hidden="true" aria-label="Лучшее" style="display: inline-block; transform: translate3d(0px, 0px, 0px);">
                                <span class="char1" aria-hidden="true" style="display: inline-block; visibility: visible;">Л</span>
                                <span class="char2" aria-hidden="true" style="display: inline-block; visibility: visible;">у</span>
                                <span class="char3" aria-hidden="true" style="display: inline-block; visibility: visible;">ч</span>
                                <span class="char4" aria-hidden="true" style="display: inline-block; visibility: visible;">ш</span>
                                <span class="char5" aria-hidden="true" style="display: inline-block; visibility: visible;">е</span>
                                <span class="char6" aria-hidden="true" style="display: inline-block; visibility: visible;">е</span>
                            </span>
                            <span class="word2" aria-hidden="true" aria-label="для" style="display: inline-block; transform: translate3d(0px, 0px, 0px);">
                                <span class="char1" aria-hidden="true" style="display: inline-block; visibility: visible;">д</span>
                                <span class="char2" aria-hidden="true" style="display: inline-block; visibility: visible;">л</span>
                                <span class="char3" aria-hidden="true" style="display: inline-block; visibility: visible;">я</span>
                            </span>
                            <span class="word3" aria-hidden="true" aria-label="лучших" style="display: inline-block; transform: translate3d(0px, 0px, 0px);">
                                <span class="char1" aria-hidden="true" style="display: inline-block; visibility: visible;">л</span>
                                <span class="char2" aria-hidden="true" style="display: inline-block; visibility: visible;">у</span>
                                <span class="char3" aria-hidden="true" style="display: inline-block; visibility: visible;">ч</span>
                                <span class="char4" aria-hidden="true" style="display: inline-block; visibility: visible;">ш</span>
                                <span class="char5" aria-hidden="true" style="display: inline-block; visibility: visible;">и</span>
                                <span class="char6" aria-hidden="true" style="display: inline-block; visibility: visible;">х</span>
                            </span>
                        </span>
                            <ul class="texts" style="display: none;">
                                <li class="current">Лучшее для лучших</li>
                            </ul>
                        </h2>
                    </div>
                </div>
                <div class="catalog">
                    <div id="production" class="container">
                        <h3>Мы предлагаем</h3>
                        <a href="<?php echo trailingslashit( site_url() ); ?>roofing-materials/" class="catalog-wrap a-link flex wow fadeInUp" data-wow-duration="2s" style="visibility: visible; animation-duration: 2s; animation-name: fadeInUp;">
                            <div class="catalog-content">
                                <h4 class="wow slideInLeft" data-wow-duration="2s" style="visibility: visible; animation-duration: 2s; animation-name: slideInLeft;">Кровля</h4>
                                <p>
                                    <span>Самый большой выбор кровельных материалов в Украине: кровельный профнастил, натуральная черепица, битумная черепица, металлочерепица, композитная черепица, цементно-песчаная черепи</span>
                                    <span>...</span>
                                </p>
                                <button class="all_btn">Подробнее</button>
                            </div>
                            <div class="catalog-img">
                                <img class="wow pulse" data-wow-duration="2s" src="https://d3l3qn7kx5ewr2.cloudfront.net/wp-content/uploads/20190409122932/s1.jpg" style="visibility: visible; animation-duration: 2s; animation-name: pulse;">
                            </div>
                        </a>
                        <a href="<?php echo trailingslashit( site_url() ); ?>facade-materials/" class="catalog-wrap flex a-link wow fadeInUp" data-wow-duration="2s" style="visibility: visible; animation-duration: 2s; animation-name: fadeInUp;">
                            <div class="catalog-content">
                                <h4 class="wow slideInRight" data-wow-duration="2s" style="visibility: visible; animation-duration: 2s; animation-name: slideInRight;">Фасад</h4>
                                <p>
                                    <span>Широкий ассортимент долговечных, термоустойчивых, вентилируемых отделочных материалов для фасада дома: стеновой профнастил, сэндвич, клинкерный кирпич, гибкий кирпич. Используйте д</span>
                                    <span>...</span>
                                </p>
                                <button class="all_btn">Подробнее</button>
                            </div>
                            <div class="catalog-img">
                                <img class="wow pulse" data-wow-duration="2s" src="https://d3l3qn7kx5ewr2.cloudfront.net/wp-content/uploads/20190409122837/fasad.jpg" style="visibility: visible; animation-duration: 2s; animation-name: pulse;">
                            </div>
                        </a>
                        <a href="<?php echo trailingslashit( site_url() ); ?>roof-drainage/" class="catalog-wrap a-link flex wow fadeInUp" data-wow-duration="2s" style="visibility: hidden; animation-duration: 2s; animation-name: none;">
                            <div class="catalog-content border">
                                <h4 class=" wow slideInLeft" data-wow-duration="2s" style="visibility: hidden; animation-duration: 2s; animation-name: none;">Водосточные системы</h4>
                                <p>
                                    <span>Реализуем водосточные системы из пластика и металла от ведущих производителей торговых марок Bryza, Ines, Profil, Аквадук. Точная геометрическая форма, устойчивость к коррозии, пер</span>
                                    <span>...</span>
                                </p>
                                <button class="all_btn">Подробнее</button>
                            </div>
                            <div class="catalog-img">
                                <img class="wow pulse" data-wow-duration="2s" src="https://d3l3qn7kx5ewr2.cloudfront.net/wp-content/uploads/20190409122848/k2.jpg" style="visibility: hidden; animation-duration: 2s; animation-name: none;">
                            </div>
                        </a>
                        <a href="<?php echo trailingslashit( site_url() ); ?>mansard-windows/" class="catalog-wrap flex a-link wow fadeInUp" data-wow-duration="2s" style="visibility: hidden; animation-duration: 2s; animation-name: none;">
                            <div class="catalog-content">
                                <h4 class="wow slideInRight" data-wow-duration="2s" style="visibility: hidden; animation-duration: 2s; animation-name: none;">Мансардные окна</h4>
                                <p>
                                    <span>Предлагаем мансардные окна безупречного качества торговых марок  Fakro, Velux, обеспечивающие достаточную вентиляцию, освещение чердачного помещения. Различных модификаций, одно-, </span>
                                    <span>...</span>
                                </p>
                                <button class="all_btn">Подробнее</button>
                            </div>
                            <div class="catalog-img">
                                <img class="wow pulse" data-wow-duration="2s" src="https://d3l3qn7kx5ewr2.cloudfront.net/wp-content/uploads/20190409122850/k3.jpg" style="visibility: hidden; animation-duration: 2s; animation-name: none;">
                            </div>
                        </a>
                        <a href="<?php echo trailingslashit( site_url() ); ?>ancillary-materials/" class="catalog-wrap flex a-link wow fadeInUp" data-wow-duration="2s" style="visibility: hidden; animation-duration: 2s; animation-name: none;">
                            <div class="catalog-content">
                                <h4 class="wow slideInRight" data-wow-duration="2s" style="visibility: hidden; animation-duration: 2s; animation-name: none;">Сопутствующие товары</h4>
                                <p>
                                    <span>У нас можно купить все расходные материалы, которые понадобятся для отделки фасада, при кровельных работах, креплении водосточных систем, монтаже мансардных окон, укладке тротуарно</span>
                                    <span>...</span>
                                </p>
                                <button class="all_btn">Подробнее</button>
                            </div>
                            <div class="catalog-img">
                                <img class="wow pulse" data-wow-duration="2s" src="https://d3l3qn7kx5ewr2.cloudfront.net/wp-content/uploads/20190409122911/other.jpg" style="visibility: hidden; animation-duration: 2s; animation-name: none;">
                            </div>
                        </a>
                    </div>
                </div>
                <div class="about">
                    <div class="container">
                        <h3>О нас</h3>
                        <div class="about-wrap flex">
                            <div class="about-content">
                                <p class=" wow slideInLeft" data-wow-duration="2s" style="visibility: hidden; animation-duration: 2s; animation-name: none;">У нас представлен большой выбор кровельных, отделочных и сопутствующих материалов по доступным ценам для строительства частных домов,  коммерческих,  муниципальных,  офисных зданий.</p>
                                <p class=" wow slideInLeft" data-wow-duration="2s" style="visibility: hidden; animation-duration: 2s; animation-name: none;">Строительные материалы прошли оценку качества и соответствуют стандартам безопасности, прочности, одобрены для использования на фасадах, крышах производственных, жилых и нежилых построек.</p>
                                <p class=" wow slideInLeft" data-wow-duration="2s" style="visibility: hidden; animation-duration: 2s; animation-name: none;">Поможем вам выбрать подходящий материал в зависимости от ваших требований к техническим характеристикам, внешнему виду и типу постройки.</p>
                            </div>
                            <div class="about-img">
                                <img class="wow pulse" data-wow-duration="2s" src="https://d3l3qn7kx5ewr2.cloudfront.net/wp-content/uploads/20190409122950/s9.jpg" alt="img" style="visibility: hidden; animation-duration: 2s; animation-name: none;">
                            </div>
                        </div>
                        <div class="about-wrap flex">
                            <div class="about-img">
                                <img class="wow pulse" data-wow-duration="2s" src="https://d3l3qn7kx5ewr2.cloudfront.net/wp-content/uploads/20190409122945/s7.jpg" alt="img" style="visibility: hidden; animation-duration: 2s; animation-name: none;">
                            </div>
                            <div class="about-content">
                                <p class="wow slideInRight" data-wow-duration="2s" style="visibility: hidden; animation-duration: 2s; animation-name: none;">Квалифицированный личный консультант проведёт точный подсчёт сметы на стройматериалы, сопутствующие товары.</p>
                                <p class="wow slideInRight" data-wow-duration="2s" style="visibility: hidden; animation-duration: 2s; animation-name: none;">Более 500 наименований товаров для ремонтно-строительных работ по реконструкции, строительству крыш, отделки зданий.</p>
                                <p class="wow slideInRight" data-wow-duration="2s" style="visibility: hidden; animation-duration: 2s; animation-name: none;">Являемся надёжным поставщиком строительных материалов от ведущих производителей.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

<?php else: ?>

    <div class="wrap">
        <div id="app">
            <div>
                <div class="promo">
                    <div class="slider">
                        <span class="slider_section" style="background: url('https://d3l3qn7kx5ewr2.cloudfront.net/wp-content/uploads/20190409123002/slide1-min.jpg'); width: 1358px; position: relative; left: 0px; top: 0px; z-index: 998; opacity: 0; transition: opacity 2000ms linear 0s;" aria-hidden="true" tabindex="-1" role="option" aria-describedby="slick-slide00"></span>
                        <span class="slider_section" style="background: url('https://d3l3qn7kx5ewr2.cloudfront.net/wp-content/uploads/20190409122957/sl3.jpg'); width: 1358px; position: relative; left: -1358px; top: 0px; z-index: 998; opacity: 0; transition: opacity 2000ms linear 0s;" aria-hidden="true" tabindex="-1" role="option" aria-describedby="slick-slide01"></span>
                        <span class="slider_section" style="background: url('https://d3l3qn7kx5ewr2.cloudfront.net/wp-content/uploads/20190409122947/s8.jpg'); width: 1358px; position: relative; left: -2716px; top: 0px; z-index: 999; opacity: 1;" aria-hidden="false" tabindex="-1" role="option" aria-describedby="slick-slide02"></span>
                        <span class="slider_section" style="background: url('https://d3l3qn7kx5ewr2.cloudfront.net/wp-content/uploads/20190409122958/sl4-min.jpg'); width: 1358px; position: relative; left: -4074px; top: 0px; z-index: 998; opacity: 0; transition: opacity 2000ms linear 0s;" aria-hidden="true" tabindex="-1" role="option" aria-describedby="slick-slide03"></span>
                        <span class="slider_section" style="background: url('https://d3l3qn7kx5ewr2.cloudfront.net/wp-content/uploads/20190409122959/sl5-min.jpg'); width: 1358px; position: relative; left: -5432px; top: 0px; z-index: 998; opacity: 0; transition: opacity 2000ms linear 0s;" aria-hidden="true" tabindex="-1" role="option" aria-describedby="slick-slide04"></span>
                        <span class="slider_section" style="background: url('https://d3l3qn7kx5ewr2.cloudfront.net/wp-content/uploads/20190409122956/sl1-min.jpg'); width: 1358px; position: relative; left: -6790px; top: 0px; z-index: 998; opacity: 0; transition: opacity 2000ms linear 0s;" aria-hidden="true" tabindex="-1" role="option" aria-describedby="slick-slide05"></span>
                        <span class="slider_section" style="background: url('https://d3l3qn7kx5ewr2.cloudfront.net/wp-content/uploads/20190409122957/sl2-min.jpg'); width: 1358px; position: relative; left: -8148px; top: 0px; z-index: 998; opacity: 0; transition: opacity 2000ms linear 0s;" aria-hidden="true" tabindex="-1" role="option" aria-describedby="slick-slide06"></span>
                    </div>
                    <div class="promo-content">
                        <h1 class="front">КСМ "Асгард" - покрівельні та фасадні матеріали</h1>
                        <h3 class="front" style="">Можливість виготовлення замовлення за 1 день</h3>
                        <h3 class="front" style="">Ціни без жодних комісій</h3>
                        <h3 class="front" style="">Безкоштовна професійна консультація</h3>
                        <h3 class="front" style="">Гарантія від виробника</h3>
                        <h3 class="front" style="">Доставка по всій Україні</h3>
                        <h3 class="front" style="">Сучасні технології</h3>
                        <h3 class="front" style="">Команда майстрів своєї справи</h3>
                    </div>
                </div>
                <div class="mobile-about">
                    <h3 class="" style="">Можливість виготовлення замовлення за 1 день</h3>
                    <h3 class="" style="">Ціни без жодних комісій</h3>
                    <h3 class="" style="">Безкоштовна професійна консультація</h3>
                    <h3 class="" style="">Гарантія від виробника</h3>
                    <h3 class="" style="">Доставка по всій Україні</h3>
                    <h3 class="" style="">Сучасні технології</h3>
                    <h3 class="" style="">Команда майстрів своєї справи</h3>
                </div>
                <div class="catalog">
                    <div id="production" class="container">
                        <h2>КСМ "Асгард" пропонує Вам</h2>
                        <a href="<?php echo trailingslashit( site_url() ); ?>roofing-materials/" class="catalog-wrap a-link flex wow fadeInUp" data-wow-duration="2s" style="visibility: visible; animation-duration: 2s; animation-name: fadeInUp;">
                            <div class="catalog-content">
                                <h4 class="wow slideInLeft" data-wow-duration="2s" style="visibility: visible; animation-duration: 2s; animation-name: slideInLeft;">Покрівля</h4>
                                <p>
                                    <span>Найбільший вибір покрівельних матеріалів в Україні. Покрівельний профнастіл, сендвіч-панелі, черепиця в асортименті, сланцева покрівля, декоративні покрівельні матеріали.</span>
                                </p>
                                <button class="all_btn">Детальніше</button>
                            </div>
                            <div class="catalog-img">
                                <img class="wow pulse" data-wow-duration="2s" src="https://d3l3qn7kx5ewr2.cloudfront.net/wp-content/uploads/20190409122932/s1.jpg" style="visibility: visible; animation-duration: 2s; animation-name: pulse;">
                            </div>
                        </a>
                        <a href="<?php echo trailingslashit( site_url() ); ?>facade-materials/" class="catalog-wrap flex a-link wow fadeInUp" data-wow-duration="2s" style="visibility: visible; animation-duration: 2s; animation-name: fadeInUp;">
                            <div class="catalog-content">
                                <h4 class="wow slideInRight" data-wow-duration="2s" style="visibility: visible; animation-duration: 2s; animation-name: slideInRight;">Фасади</h4>
                                <p>
                                    <span>Великий асортимент надійних та довговічних фасадних матеріалів. Фасадні сендвіч панелі, стіновий профнастіл, фасадний сайдинг, гнучка цегла, широкий вибір фасадної плитки, фарба, герметики.</span>
                                </p>
                                <button class="all_btn">Детальніше</button>
                            </div>
                            <div class="catalog-img">
                                <img class="wow pulse" data-wow-duration="2s" src="https://d3l3qn7kx5ewr2.cloudfront.net/wp-content/uploads/20190409122837/fasad.jpg" style="visibility: visible; animation-duration: 2s; animation-name: pulse;">
                            </div>
                        </a>
                        <a href="<?php echo trailingslashit( site_url() ); ?>roof-drainage/" class="catalog-wrap a-link flex wow fadeInUp" data-wow-duration="2s" style="visibility: hidden; animation-duration: 2s; animation-name: none;">
                            <div class="catalog-content border">
                                <h4 class=" wow slideInLeft" data-wow-duration="2s" style="visibility: hidden; animation-duration: 2s; animation-name: none;">Водостічні системи</h4>
                                <p>
                                    <span>Широкий спектр водостічних систем на всі випадки. Пластикові та металеві водостоки, прямокутного перетину та округлі, приховані водостічні системи, а також кріплення і комплектуючі.</span>
                                </p>
                                <button class="all_btn">Детальніше</button>
                            </div>
                            <div class="catalog-img">
                                <img class="wow pulse" data-wow-duration="2s" src="https://d3l3qn7kx5ewr2.cloudfront.net/wp-content/uploads/20190409122848/k2.jpg" style="visibility: hidden; animation-duration: 2s; animation-name: none;">
                            </div>
                        </a>
                        <a href="<?php echo trailingslashit( site_url() ); ?>mansard-windows/" class="catalog-wrap flex a-link wow fadeInUp" data-wow-duration="2s" style="visibility: hidden; animation-duration: 2s; animation-name: none;">
                            <div class="catalog-content">
                                <h4 class="wow slideInRight" data-wow-duration="2s" style="visibility: hidden; animation-duration: 2s; animation-name: none;">Мансардні вікна</h4>
                                <p>
                                    <span>Різноманіття мансардних вікон та люків. Мансардні вікна від провідних виробників - Felux та Fakro.</span>
                                </p>
                                <button class="all_btn">Детальніше</button>
                            </div>
                            <div class="catalog-img">
                                <img class="wow pulse" data-wow-duration="2s" src="https://d3l3qn7kx5ewr2.cloudfront.net/wp-content/uploads/20190409122850/k3.jpg" style="visibility: hidden; animation-duration: 2s; animation-name: none;">
                            </div>
                        </a>
                        <a href="<?php echo trailingslashit( site_url() ); ?>ancillary-materials/" class="catalog-wrap flex a-link wow fadeInUp" data-wow-duration="2s" style="visibility: hidden; animation-duration: 2s; animation-name: none;">
                            <div class="catalog-content">
                                <h4 class="wow slideInRight" data-wow-duration="2s" style="visibility: hidden; animation-duration: 2s; animation-name: none;">Супутні товари</h4>
                                <p>
                                    <span>Абсолютно все, що може знадобитись для установки і монтажу фасадних і покрівельних матеріалів. Всілякі кріпильні та гнучкі елементи власного виробництва, паро- та гідроізоляція.</span>
                                </p>
                                <button class="all_btn">Детальніше</button>
                            </div>
                            <div class="catalog-img">
                                <img class="wow pulse" data-wow-duration="2s" src="https://d3l3qn7kx5ewr2.cloudfront.net/wp-content/uploads/20190409122911/other.jpg" style="visibility: hidden; animation-duration: 2s; animation-name: none;">
                            </div>
                        </a>
                        <a href="#" class="catalog-wrap flex a-link wow fadeInUp" data-wow-duration="2s" style="visibility: hidden; animation-duration: 2s; animation-name: none;">
                            <div class="catalog-content">
                                <h4 class="wow slideInRight" data-wow-duration="2s" style="visibility: hidden; animation-duration: 2s; animation-name: none;">Матеріали на замовлення</h4>
                                <p>
                                    <span>Незвичайні рішення, нестандартні розміри - в стислі терміни та з доставкою по всій Україні</span>
                                </p>
                            </div>
                            <div class="catalog-img">
                                <img class="wow pulse" data-wow-duration="2s" src="https://d3l3qn7kx5ewr2.cloudfront.net/wp-content/uploads/20190409122911/other.jpg" style="visibility: hidden; animation-duration: 2s; animation-name: none;">
                            </div>
                        </a>
                    </div>
                </div>
                <div class="about">
                    <div class="container">
                        <h2>КСМ "Асгард" це</h2>
                        <div class="about-wrap flex">
                            <div class="about-content">
                                <h3 class="short wow slideInLeft" data-wow-duration="2s" style="visibility: hidden; animation-duration: 2s; animation-name: none;">Досвід та професіоналізм</h3>
                                <p class="wow slideInLeft long" data-wow-duration="2s" style="visibility: hidden; animation-duration: 2s; animation-name: none;">КСМ "Асгард" - команда досвідчених професіоналів в галузі покрівельних та фасадних матеріалів. Співпрацюючи з нами, Ви отримуєте кваліфікованого спеціаліста на кожному рівні кооперації. Починаючи з виробництва, де створюють Ваші майбутні дах та стіни, а їх якість перевіряють експерти з багаторічним досвідом. І закінчуючи консультантами, що завжди готові прийти на допомогу і знайти оптимальний варіант вирішення будь-якої, навіть нетривіальної задачі.</p>
                                <h3 class="short wow slideInLeft" data-wow-duration="2s" style="visibility: hidden; animation-duration: 2s; animation-name: none;">Сумлінний партнер</h3>
                                <p class="wow slideInLeft long" data-wow-duration="2s" style="visibility: hidden; animation-duration: 2s; animation-name: none;">Нашими послугами користуються тисячі клієнтів по всій Україні. Завдяки оперативності виконання замовлень, незвичайно високим стандартам якості, а також персоналізованому підходу до кожного, КСМ "Асгард" має репутацію надійного та сумлінного партнера. Секрет дуже простий — виконуючи замовлення, ми робимо все від нас залежне для того, щоб замовник ніколи не пошкодував про співпрацю.</p>
                            </div>
                            <div class="about-img">
                                <img class="wow pulse" data-wow-duration="2s" src="https://d3l3qn7kx5ewr2.cloudfront.net/wp-content/uploads/20190409122950/s9.jpg" alt="img" style="visibility: hidden; animation-duration: 2s; animation-name: none;">
                            </div>
                        </div>
                        <div class="about-wrap flex">
                            <div class="about-img">
                                <img class="wow pulse" data-wow-duration="2s" src="https://d3l3qn7kx5ewr2.cloudfront.net/wp-content/uploads/20190409122945/s7.jpg" alt="img" style="visibility: hidden; animation-duration: 2s; animation-name: none;">
                            </div>
                            <div class="about-content">
                                <h3 class="short wow slideInLeft" data-wow-duration="2s" style="visibility: hidden; animation-duration: 2s; animation-name: none;">Чесна ціна</h3>
                                <p class="wow slideInLeft long" data-wow-duration="2s" style="visibility: hidden; animation-duration: 2s; animation-name: none;">Власне виробництво дозволяє КСМ "Асгард" втілювати привабливу цінову політику. Крім того, ми напряму співпрацюємо з відомими в світі покрівельних та фасадних матеріалів брендами, такими як Velux, Fakro, Metrotile та багатьма іншими. Відсутність посередників, — цих зайвих ланок в ланцюзі виробник - кінцевий клієнт, — дозволяє нам мати набагато більш привабливі ціни, ніж конкуренти на ринку.</p>
                                <h3 class="short wow slideInRight" data-wow-duration="2s" style="visibility: hidden; animation-duration: 2s; animation-name: none;">Доставка по всій Україні</h3>
                                <p class="wow slideInRight long" data-wow-duration="2s" style="visibility: hidden; animation-duration: 2s; animation-name: none;">Власний автопарк дозволяє КСМ "Асгард" охоплювати не тільки Київ та область, але й найвіддаленіші куточки нашої Батьківщини. Відсутність необхідності оплачувати доставку стороннім компаніям позитивно впливає на формування кінцевої ціни продукту. Такий підхід дозволяє не тільки доставляти покрівельні та фасадні матеріали по всій Україні в стислі терміни. Це ще й надає КСМ "Асгард" конкурентної переваги відносно місцевих виробників. І абсолютно точно робить партнером #1 з точки зору співвідношення ціна/якість.</p>
                            </div>
                        </div>
                        <div class="about-wrap flex">
                            <div class="about-content wide">
                                <h3 class="short wow slideInRight" data-wow-duration="2s" style="visibility: hidden; animation-duration: 2s; animation-name: none;">Безкоштовна кваліфікована допомога в виборі покрівельних та фасадних матеріалів</h3>
                                <p class="wow slideInRight long" data-wow-duration="2s" style="visibility: hidden; animation-duration: 2s; animation-name: none;">Спеціалісти КСМ "Асгард" проводять професійні консультації щодо вибору покрівельних матеріалів та фасадних систем. Ця послуга є абсолютно безкоштовною. З'ясуйте, який підхід стане найкращим саме у Вашому випадку, на що звернути увагу під час вибору комплектуючих, переваги тих чи інших матеріалів, а також як проконтролювати процес виконання монтажних робіт підрядниками. Телефонуйте за номерами, що вказані нижче.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

<?php
endif;
get_footer();
