<div class="address-price" data-wow-duration="2s" style="visibility: visible; animation-duration: 2s; animation-name: fadeInUp;">
    <p>Уточнити чи дізнатися ціну можна по телефону:</p>
    <p><a href="tel:0678344242">067 834 42 42</a></p>
    <p><a href="tel:0955952437">095 595 24 37</a></p>
    <p><a href="tel:0733193111">073 319 31 11</a></p>
    <p>Наш e-mail:</p>
    <p><a href="mailto:asgard.ksm@gmail.com">asgard.ksm@gmail.com</a></p>
</div>