<div class="decor_elem palitra">
    <h2 style="margin-bottom:0;">Палітра:</h2>
    <p style="margin-bottom:30px;">Кольори на сайті і наживо можуть дещо відрізнятися.</p>
    <div class="flex">
        <div class="decor-wrap wow fadeInUp" data-wow-duration="2s" style="visibility: visible; animation-duration: 2s; animation-name: fadeInUp;">
            <div style="background-color: rgb(229,190,1);" class="decor-img"></div>
            <div class="decor-cont">
                <h3 class="wow fadeIn" data-wow-duration="2s" style="visibility: visible; animation-duration: 2s; animation-name: fadeIn;">RAL 1003</h3>
            </div>
        </div>
        <div class="decor-wrap wow fadeInUp" data-wow-duration="2s" style="visibility: visible; animation-duration: 2s; animation-name: fadeInUp;">
            <div style="background-color: rgb(230,214,144);" class="decor-img"></div>
            <div class="decor-cont">
                <h3 class="wow fadeIn" data-wow-duration="2s" style="visibility: visible; animation-duration: 2s; animation-name: fadeIn;">RAL 1015</h3>
            </div>
        </div>
        <div class="decor-wrap wow fadeInUp" data-wow-duration="2s" style="visibility: visible; animation-duration: 2s; animation-name: fadeInUp;">
            <div style="background-color: rgb(94,33,41);" class="decor-img"></div>
            <div class="decor-cont">
                <h3 class="wow fadeIn" data-wow-duration="2s" style="visibility: visible; animation-duration: 2s; animation-name: fadeIn;">RAL 3005</h3>
            </div>
        </div>
        <div class="decor-wrap wow fadeInUp" data-wow-duration="2s" style="visibility: visible; animation-duration: 2s; animation-name: fadeInUp;">
            <div style="background-color: rgb(120,31,25);" class="decor-img"></div>
            <div class="decor-cont">
                <h3 class="wow fadeIn" data-wow-duration="2s" style="visibility: visible; animation-duration: 2s; animation-name: fadeIn;">RAL 3011</h3>
            </div>
        </div>
        <div class="decor-wrap wow fadeInUp" data-wow-duration="2s" style="visibility: visible; animation-duration: 2s; animation-name: fadeInUp;">
            <div style="background-color: rgb(30,45,110);" class="decor-img"></div>
            <div class="decor-cont">
                <h3 class="wow fadeIn" data-wow-duration="2s" style="visibility: visible; animation-duration: 2s; animation-name: fadeIn;">RAL 5005</h3>
            </div>
        </div>
        <div class="decor-wrap wow fadeInUp" data-wow-duration="2s" style="visibility: visible; animation-duration: 2s; animation-name: fadeInUp;">
            <div style="background-color: rgb(47,69,56);" class="decor-img"></div>
            <div class="decor-cont">
                <h3 class="wow fadeIn" data-wow-duration="2s" style="visibility: visible; animation-duration: 2s; animation-name: fadeIn;">RAL 6005</h3>
            </div>
        </div>
        <div class="decor-wrap wow fadeInUp" data-wow-duration="2s" style="visibility: visible; animation-duration: 2s; animation-name: fadeInUp;">
            <div style="background-color: rgb(46,58,35);" class="decor-img"></div>
            <div class="decor-cont">
                <h3 class="wow fadeIn" data-wow-duration="2s" style="visibility: visible; animation-duration: 2s; animation-name: fadeIn;">RAL 6020</h3>
            </div>
        </div>
        <div class="decor-wrap wow fadeInUp" data-wow-duration="2s" style="visibility: visible; animation-duration: 2s; animation-name: fadeInUp;">
            <div style="background-color: rgb(150,153,146);" class="decor-img"></div>
            <div class="decor-cont">
                <h3 class="wow fadeIn" data-wow-duration="2s" style="visibility: visible; animation-duration: 2s; animation-name: fadeIn;">RAL 7004</h3>
            </div>
        </div>
        <div class="decor-wrap wow fadeInUp" data-wow-duration="2s" style="visibility: visible; animation-duration: 2s; animation-name: fadeInUp;">
            <div style="background-color: rgb(71,74,81);" class="decor-img"></div>
            <div class="decor-cont">
                <h3 class="wow fadeIn" data-wow-duration="2s" style="visibility: visible; animation-duration: 2s; animation-name: fadeIn;">RAL 7024</h3>
            </div>
        </div>
        <div class="decor-wrap wow fadeInUp" data-wow-duration="2s" style="visibility: visible; animation-duration: 2s; animation-name: fadeInUp;">
            <div style="background-color: rgb(142,64,42);" class="decor-img"></div>
            <div class="decor-cont">
                <h3 class="wow fadeIn" data-wow-duration="2s" style="visibility: visible; animation-duration: 2s; animation-name: fadeIn;">RAL 8004</h3>
            </div>
        </div>
        <div class="decor-wrap wow fadeInUp" data-wow-duration="2s" style="visibility: visible; animation-duration: 2s; animation-name: fadeInUp;">
            <div style="background-color: rgb(69,50,46);" class="decor-img"></div>
            <div class="decor-cont">
                <h3 class="wow fadeIn" data-wow-duration="2s" style="visibility: visible; animation-duration: 2s; animation-name: fadeIn;">RAL 8017</h3>
            </div>
        </div>
        <div class="decor-wrap wow fadeInUp" data-wow-duration="2s" style="visibility: visible; animation-duration: 2s; animation-name: fadeInUp;">
            <div style="background-color: rgb(64,58,58);" class="decor-img"></div>
            <div class="decor-cont">
                <h3 class="wow fadeIn" data-wow-duration="2s" style="visibility: visible; animation-duration: 2s; animation-name: fadeIn;">RAL 8019</h3>
            </div>
        </div>
        <div class="decor-wrap wow fadeInUp" data-wow-duration="2s" style="visibility: visible; animation-duration: 2s; animation-name: fadeInUp;">
            <div style="background-color: rgb(244,244,244);" class="decor-img"></div>
            <div class="decor-cont">
                <h3 class="wow fadeIn" data-wow-duration="2s" style="visibility: visible; animation-duration: 2s; animation-name: fadeIn;">RAL 9003</h3>
            </div>
        </div>
        <div class="decor-wrap wow fadeInUp" data-wow-duration="2s" style="visibility: visible; animation-duration: 2s; animation-name: fadeInUp;">
            <div style="background-color: rgb(10,10,10);" class="decor-img"></div>
            <div class="decor-cont">
                <h3 class="wow fadeIn" data-wow-duration="2s" style="visibility: visible; animation-duration: 2s; animation-name: fadeIn;">RAL 9005</h3>
            </div>
        </div>
        <div class="decor-wrap wow fadeInUp" data-wow-duration="2s" style="visibility: visible; animation-duration: 2s; animation-name: fadeInUp;">
            <div style="background-color: rgb(165,165,165);" class="decor-img"></div>
            <div class="decor-cont">
                <h3 class="wow fadeIn" data-wow-duration="2s" style="visibility: visible; animation-duration: 2s; animation-name: fadeIn;">RAL 9006</h3>
            </div>
        </div>
        <div class="decor-wrap wow fadeInUp" data-wow-duration="2s" style="visibility: visible; animation-duration: 2s; animation-name: fadeInUp;">
            <div class="decor-img">
                <img src="https://d3l3qn7kx5ewr2.cloudfront.net/wp-content/uploads/20190409123012/zinc_prof.jpg" alt="Цинк">
                <div class="decor-about"></div>
            </div>
            <div class="decor-cont">
                <h3 class="wow fadeIn" data-wow-duration="2s" style="visibility: visible; animation-duration: 2s; animation-name: fadeIn;">Цинк</h3>
            </div>
        </div>
        <div class="decor-wrap wow fadeInUp" data-wow-duration="2s" style="visibility: visible; animation-duration: 2s; animation-name: fadeInUp;">
            <div class="decor-img">
                <img src="https://d3l3qn7kx5ewr2.cloudfront.net/wp-content/uploads/20190409122928/profnastil-tem-derevo.png" alt="Темне дерево">
                <div class="decor-about"></div>
            </div>
            <div class="decor-cont">
                <h3 class="wow fadeIn small" data-wow-duration="2s" style="visibility: visible; animation-duration: 2s; animation-name: fadeIn;">Темне дерево</h3>
            </div>
        </div>
        <div class="decor-wrap wow fadeInUp" data-wow-duration="2s" style="visibility: visible; animation-duration: 2s; animation-name: fadeInUp;">
            <div class="decor-img">
                <img src="https://d3l3qn7kx5ewr2.cloudfront.net/wp-content/uploads/20190409122844/golden-oak-prof.jpg" alt="Золотий дуб">
                <div class="decor-about"></div>
            </div>
            <div class="decor-cont">
                <h3 class="wow fadeIn small" data-wow-duration="2s" style="visibility: visible; animation-duration: 2s; animation-name: fadeIn;">Золотий дуб</h3>
            </div>
        </div>
        <div class="decor-wrap wow fadeInUp" data-wow-duration="2s" style="visibility: visible; animation-duration: 2s; animation-name: fadeInUp;">
            <div class="decor-img">
                <img src="https://d3l3qn7kx5ewr2.cloudfront.net/wp-content/uploads/20190409122909/olha-prof.jpg" alt="Вільха">
                <div class="decor-about"></div>
            </div>
            <div class="decor-cont">
                <h3 class="wow fadeIn small" data-wow-duration="2s" style="visibility: visible; animation-duration: 2s; animation-name: fadeIn;">Вільха</h3>
            </div>
        </div>
        <div class="decor-wrap wow fadeInUp" data-wow-duration="2s" style="visibility: visible; animation-duration: 2s; animation-name: fadeInUp;">
            <div class="decor-img">
                <img src="https://d3l3qn7kx5ewr2.cloudfront.net/wp-content/uploads/20190409123004/stone-prof.jpg" alt="Камінь">
                <div class="decor-about"></div>
            </div>
            <div class="decor-cont">
                <h3 class="wow fadeIn small" data-wow-duration="2s" style="visibility: visible; animation-duration: 2s; animation-name: fadeIn;">Камінь</h3>
            </div>
        </div>
    </div>
    <?php get_template_part( 'template-parts/page/after-products', '' ); ?>
</div>