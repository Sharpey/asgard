<?php
/**
 * Template Name: Contacts
 */

get_header();
?>

<div class="wrap">

    <div id="app">
        <div>
            <div class="promo-all">
                <div class="promo-img" style="background:url(https://d3l3qn7kx5ewr2.cloudfront.net/wp-content/uploads/20190409122943/s6.jpg) no-repeat center/cover;">
                    <div class="promo-all-cont container">
                        <h1 class="wow bounceInDown" data-wow-duration="2s" style="visibility: visible; animation-duration: 2s; animation-name: bounceInDown;"><?php the_title(); ?></h1>
                    </div>
                </div>
                <div class="container"></div>
            </div>
            <div class="contacts">
                <div class="container flex">
                    <div class="contacts-wrap">
                        <h3>Наші контакти</h3>
                        <div class="contacts-adress wow slideInLeft" data-wow-duration="1s" style="visibility: visible; animation-duration: 1s; animation-name: slideInLeft;">
                            <h4>Наша адреса:</h4>
                            <p>м. Київ, пр. Степана Бандери 21</p>
                        </div>
                        <div class="contacts-mode">
                            <h4>Графік роботи:</h4>
                            <p>Пн-Пт: 9:00-18:00</p>
                            <p>Сб-Нд: в телефонному режимі</p>
                        </div>
                        <div class="contacts-tel wow slideInLeft" data-wow-duration="1s" style="visibility: visible; animation-duration: 1s; animation-name: slideInLeft;">
                            <h4>Наші телефони:</h4>
                            <a href="tel:0678344242">067 834 42 42</a>
                            <a href="tel:0955952437">095 595 24 37 </a>
                            <a href="tel:0733193111">073 319 31 11</a>
                        </div>
                        <div class="contacts-email wow slideInLeft" data-wow-duration="1s" style="visibility: visible; animation-duration: 1s; animation-name: slideInLeft;">
                            <h4>Наш E-mail:</h4>
                            <a href="mailto:asgard.ksm@gmail.com">asgard.ksm@gmail.com</a>
                        </div>
                        <form method="post" class="footer-form wow slideInLeft" data-wow-duration="1s" style="visibility: visible; animation-duration: 1s; animation-name: slideInLeft;">
                            <input type="text" name="u_name" class="modal-text" placeholder="Ім'я">
                            <input type="number" name="u_phone" class="modal-phone" placeholder="Телефон">
                            <button class="all_btn">Надіслати</button>
                            <br/>
                            <br/>
                            <span class="error_inp"></span>
                        </form>
                    </div>
                </div>
                <iframe class="contacts-map" src="https://www.google.com/maps/d/u/0/embed?mid=1BNivvRWmsM-BFKMt8BmTS8WD1HZqJelL"></iframe>
            </div>
        </div>
    </div>

</div>


<?php
get_footer("contacts");
