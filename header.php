<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
    <?php if ( is_front_page() ) : ?>
    <meta name="google-site-verification" content="24KF3DN5iBP38cxFfc8acPLxone9ZgdvJZ9g_ExivqU" />
    <?php endif; ?>
	<meta charset="<?php bloginfo( 'charset' ); ?>" />
	<meta name="viewport" content="width=device-width, initial-scale=1" />
	<link rel="profile" href="https://gmpg.org/xfn/11" />
    <?php
        $page_title = get_post_meta( $post->ID, '_yoast_wpseo_title', true );
        if ( empty( $page_title ) ) {
            $page_title = get_the_title();
        }
    ?>

    <!-- Google Tag Manager -->
    <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
            new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
            j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
            'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
        })(window,document,'script','dataLayer','GTM-PWHX3PJ');</script>
    <!-- End Google Tag Manager -->

    <title><?php echo $page_title; ?></title>
	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-PWHX3PJ"
                  height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->
<div id="page" class="site">

		<header id="masthead" class="site-header featured-image">

			<?php if ( is_singular() ) : ?>
				<div class="site-featured-image">
					<?php
						the_post_thumbnail();
						the_post();
					?>

					<?php rewind_posts(); ?>
				</div>
			<?php endif; ?>
		</header><!-- #masthead -->

	<div id="content" class="site-content">
        <div class="site-content--header">
            <div class="site-content--header-item item-logo" >
                <a href="<?php echo trailingslashit( site_url() ); ?>" class="logo route-link"></a>
            </div>
            <div class="site-content--header-item item-phone" >
                <a class="item-phone--link" href="tel:0678344242">
                    <span class="item-phone--operator-icon"><img src="https://d3l3qn7kx5ewr2.cloudfront.net/wp-content/uploads/20190916155640/Kyivstar_logo.png" /></span> 067 834 42 42</a>
                <a class="item-phone--link" href="tel:0955952437">
                    <span class="item-phone--operator-icon"><img src="https://d3l3qn7kx5ewr2.cloudfront.net/wp-content/uploads/20190916155640/Vodafone_logo.png" /></span> 095 595 24 37</a>
                <a class="item-phone--link" href="tel:0733193111">
                    <span class="item-phone--operator-icon"><img src="https://d3l3qn7kx5ewr2.cloudfront.net/wp-content/uploads/20191021160517/life_logo.png" /></span> 073 319 31 11</a>
            </div>
        </div>
		<div class="open_menu"><span>menu</span></div>
		<div class="close_menu" style="display: none;"></div>
		<!-- menu -->
		<div class="menu-nav">
			<nav class="menu-nav-wrap flex">
				<a href="<?php echo trailingslashit( site_url() ); ?>" class="all_btn route-link">Головна</a>
				<a href="<?php echo trailingslashit( site_url() ); ?>#production" class="all_btn collection-link">Продукція</a>
				<a href="<?php echo trailingslashit( site_url() ); ?>gallery" class="all_btn route-link">Галерея</a>
				<a href="<?php echo trailingslashit( site_url() ); ?>contacts" class="all_btn route-link">Контакти</a>
			</nav>
		</div>