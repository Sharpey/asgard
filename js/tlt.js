function initTextillate()
{

// $('.tlt1').textillate({
//   // селектор по умолчанию для использования нескольких текстов для анимации
//   selector: '.texts',

//   // Повтор анимации true/false
//   loop: false,
//   initialDelay: 0,
//   autoStart: true,
//   in: {
//     effect: 'flipInX',

//     delayScale: 2,
//     delay: 100,
//     sync: false,
//     shuffle: false,
//     reverse: false,
//     // sequence: false,
//     callback: function () {}
//   },
//   // задать Тип маркера для анимации (доступные типы: символ 'char' и слово 'word')
//   type: 'char'
// });

$('.tlt2').show();
$('.tlt2').textillate({
  // селектор по умолчанию для использования нескольких текстов для анимации
  selector: '.texts',

  // Повтор анимации true/false
  loop: false,

  // устанавливает минимальное время отображения текста, дальше идет исчезновение текста с указанным эффектом
  // minDisplayTime: 2000,

  // устанавливает начальную задержку перед началом анимации
  // (обратите внимание, что в зависимости от эффекта вам, возможно, придется применять вручную
  // visibility: скрытый элемент перед запуском этого плагина)
  initialDelay: 700,

  // автоматический запуск анимации true/false
  autoStart: true,

  // custom set of 'in' effects. This effects whether or not the
  // character is shown/hidden before or after an animation
  inEffects: [],

  // эффект анимации в исчезновении текста
  outEffects: [],

  // Выбор эффекта в появлении текста
  in: {
    // название эффекта
    effect: 'fadeInLeft',

    // время появления каждой буквы
    delayScale: 1.5,

    // установить задержку между символами
    delay: 50,

    // одновременное появление эффекта true/false
    sync: false,

    // рандомная последовательность символов
    // (заметим, что рандом не имеет смысла с sync = true)
    shuffle: true,

    // обратная последовательность символов
    // (заметим, что обратное не имеет смысла с sync = true)
    reverse: false,
    sequence: false,

    // callback that executes once the animation has finished
    callback: function () {}
  },

  // настройка исчезновении анимации
  out: {
    // effect: 'hinge',
    // delayScale: 1.5,
    // delay: 50,
    // sync: false,
    // shuffle: false,
    // reverse: false,
    // callback: function () {}
  },

  // callback that executes once textillate has finished
  callback: function () {},
  // задать Тип маркера для анимации (доступные типы: символ 'char' и слово 'word')
  type: 'char'
});

}