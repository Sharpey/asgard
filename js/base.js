var Path = '/';
const Url = 'includes.php';
const Mail = 'send.php';
var resData = {};
var Param = {};
var idProd, urlProd, idPage;


jQuery(window).scroll(function(){
	if(jQuery(this).scrollTop() > 100) {
		jQuery('.logo').addClass('logo-hide');
	} else {
		jQuery('.logo').removeClass('logo-hide');
	}
})

jQuery('.open_menu').click(function() {
	jQuery('.wrap').addClass('wrap_anim');
	jQuery('.menu-nav').addClass('menu_anim');
	jQuery('.open_menu').hide();
	jQuery('.close_menu').show();
	jQuery('body').css({overflow: 'hidden'});
});

jQuery('.close_menu').click(function() { closeMenu(); });

function closeMenu()
{
	jQuery('.wrap').removeClass('wrap_anim');
	jQuery('.menu-nav').removeClass('menu_anim');
	jQuery('.open_menu').show();
	jQuery('.close_menu').hide();
	jQuery('body').css({overflowY: 'auto'});
}

jQuery('.collection-link').click(function() {
	closeMenu();
	if(window.location.pathname == Path)
	{
		jQuery('html, body').delay(1000).animate({scrollTop: jQuery('.catalog').offset().top -0}, 750);
	}
});

//CONTACTS-MAP
var h = jQuery('.contacts').outerHeight();
// alert(h);
jQuery('.contacts-map').height(h);

var sendMail = function(name, phone, callback) {
	jQuery.post(Mail, {func: 'send', name: name, phone: phone}, function(response) {
		if(response == true)
		{
			jQuery('.pop-up').fadeIn().delay(2000).fadeOut();
		}
	})
};

jQuery(document).ready(function(){
	jQuery('.spinner').delay(300).fadeOut();

    jQuery(".decor-img").on("click", function(){
        var src = jQuery(this).find("img").attr("src");
        var $image_close_up = jQuery("#image-close-up");

        if ( typeof src !== "undefined" && typeof $image_close_up !== "undefined") {
            $image_close_up.find(".pop-up-cont").css("background-image", 'url("'+src+'")');
            $image_close_up.css("display", "block");
		}
    });

    jQuery("div.close-pop-up").on("click", function(){
        jQuery("#image-close-up").css("display", "none");
	});
});

jQuery("#callrequest").on("submit", function(e){
	e.preventDefault();
	var name  = jQuery("#u_name").val();
	var phone = jQuery("#u_phone").val();

	if ( typeof admin_ajax === "undefined" ) {
		jQuery("span.error_inp.footer_err").text("Произошла ошибка при отправке вашей заявки.<br/>Попробуйте позже или напишите нам на эл. почту.");
	} else {
		if ( typeof name !== "undefined" && typeof phone !== "undefined" && name.length > 0 && phone.length > 0 ) {
			jQuery.ajax({
				type: "POST",
				url: admin_ajax.url,
				data: {name:name,phone:phone,action:"callrequest"},
				success: function(res){
					if(res == true){
						jQuery("span.error_inp.footer_err").text("");
						jQuery('.pop-up').fadeIn().delay(2000).fadeOut();
					}
				}
			});
		} else{
			jQuery("span.error_inp.footer_err").text("Заполните поля!");
		}
	}
});
