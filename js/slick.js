function initSlick($) {

    $('.slider').not('.slick-initialized').slick({
       autoplay: true,
       autoplaySpeed: 5000,
       arrows: false,
       dots: false,
       fade: true,
       cssEase: 'linear',
       speed: 2000,
       draggable: false,
       pauseOnHover: false
    });

    $('.slider1').not('.slick-initialized').slick({
      arrows: false,
      dots: false,
      infinite: true,
  		slidesToShow: 1,
  		slidesToScroll: 1

    });
}
jQuery(document).ready(function(){
    initSlick(jQuery);
});

