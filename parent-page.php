<?php
/**
 * Template Name: Parental Page
 */

get_header();

$args = array(
    'post_parent' => $post->ID,
    'post_type'   => 'page',
    'numberposts' => -1,
    'post_status' => 'publish',
    'orderby'     => 'menu_order',
    'order'       => 'ASC'
);
$children = get_children( $args );
$attc     = get_field( "pd_main_image" );
$backgr   = "";

if ( !empty( $attc ) && !empty( $attc["url"] )  ) {
    $backgr = "background:url(".$attc["url"].") no-repeat center/cover;";
}

$content_before = get_field( "pd_content_before" );
$content_after = get_field( "pd_content_after" );

?>

<div class="wrap">

    <div id="app">
        <div>
            <div class="promo-all">
                <div class="promo-img" style="<?php echo $backgr; ?>">
                    <div class="promo-all-cont container">
                        <h1 class="wow bounceInDown" data-wow-duration="2s" style="visibility: visible; animation-duration: 2s; animation-name: bounceInDown;"><?php the_title(); ?></h1>
                        <p><?php echo get_the_content(); ?></p>
                    </div>
                </div>
            </div>
            <div class="collection">
                <div class="container">
                    <div class="decor_elem">
                        <?php if ( !empty( $content_before ) ) : ?>
                            <?php foreach ( $content_before as $cb ): ?>

                            <div class="flex">
                                <div class="decor-wrap text-wrap wow" style="visibility: visible; animation-duration: 2s; animation-name: fadeInUp;">
                                    <?php echo $cb["pd_cb_content_block"]; ?>
                                </div>
                            </div>

                        <?php endforeach; endif; ?>
                    </div>
                </div>
                <div class="container flex">
                    <?php
                        if ( is_array( $children ) ) :
                            foreach ( $children as $child ) :
                            $attc = get_field( "pd_main_image", $child->ID );
                    ?>
                            <a href="<?php echo get_permalink( $child ); ?>" class="collection-wrap a-link wow fadeInUp" data-wow-duration="2s" style="visibility: visible; animation-duration: 2s; animation-name: fadeInUp;">
                                <div class="collection-img">
                                    <?php if ( !empty( $attc ) && !empty( $attc["url"] ) ) : ?>
                                    <img src="<?php echo $attc["url"]; ?>" alt="<?php echo $child->post_title; ?>" >
                                    <?php endif; ?>
                                    <h2 class="collection-wrap-cont"><?php echo $child->post_title; ?></h2>
                                </div>
                            </a>
                    <?php
                            endforeach;
                        endif;
                   ?>
                </div>
                <div class="container" style="margin-top: 50px;">
                    <div class="decor_elem">
                        <?php if ( !empty( $content_after ) ) : ?>
                            <?php foreach ( $content_after as $ca ): ?>

                            <div class="flex">
                                <div class="decor-wrap text-wrap wow" style="visibility: visible; animation-duration: 2s; animation-name: fadeInUp;">
                                    <?php echo $ca["pd_ca_content_block"]; ?>
                                </div>
                            </div>

                        <?php endforeach; endif; ?>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>


<?php
get_footer();
