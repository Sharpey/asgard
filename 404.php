<?php
/**
 * Template Name: Parental Page
 */

get_header();

?>

    <div class="wrap">

        <div id="app">
            <div>
                <div class="promo-all">
                    <div class="promo-img" style="background:url(https://d3l3qn7kx5ewr2.cloudfront.net/wp-content/uploads/20190409123002/slide1-min.jpg) no-repeat center/cover;">
                        <div class="promo-all-cont container">
                            <h1 class="wow bounceInDown" data-wow-duration="2s" style="visibility: visible; animation-duration: 2s; animation-name: bounceInDown;">404</h1>
                            <p>По Вашему запросу ничего не найдено.</p>
                            <p><a href="<?php echo site_url(); ?>">Вернуться на Главную</a>.</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>


<?php
get_footer();
