<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 */

?>
</div><!-- #content -->

	<div id="footer">
		<div>
			<div class="partners">
				<div class="container">
					<h2><?php _e( "Наші партнери" ); ?></h2>
					<div class="partners-wrap flex">
						<div class="partners-img wow fadeInUp" data-wow-duration="2s" style="visibility: visible; animation-duration: 2s; animation-name: fadeInUp;">
							<span style="background:url(https://d3l3qn7kx5ewr2.cloudfront.net/wp-content/uploads/20190409122815/c1.png) no-repeat center/contain;"></span>
						</div>
						<div class="partners-img wow fadeInUp" data-wow-duration="2s" style="visibility: visible; animation-duration: 2s; animation-name: fadeInUp;">
							<span style="background:url(https://d3l3qn7kx5ewr2.cloudfront.net/wp-content/uploads/20190409122815/c2.png) no-repeat center/contain;"></span>
						</div>
						<div class="partners-img wow fadeInUp" data-wow-duration="2s" style="visibility: visible; animation-duration: 2s; animation-name: fadeInUp;">
							<span style="background:url(https://d3l3qn7kx5ewr2.cloudfront.net/wp-content/uploads/20190409122816/c3.png) no-repeat center/contain;"></span>
						</div>
						<div class="partners-img wow fadeInUp" data-wow-duration="2s" style="visibility: visible; animation-duration: 2s; animation-name: fadeInUp;">
							<span style="background:url(https://d3l3qn7kx5ewr2.cloudfront.net/wp-content/uploads/20190409122816/c4.png) no-repeat center/contain;"></span>
						</div>
						<div class="partners-img wow fadeInUp" data-wow-duration="2s" style="visibility: visible; animation-duration: 2s; animation-name: fadeInUp;">
							<span style="background:url(https://d3l3qn7kx5ewr2.cloudfront.net/wp-content/uploads/20190409122816/c5.png) no-repeat center/contain;"></span>
						</div>
						<div class="partners-img wow fadeInUp" data-wow-duration="2s" style="visibility: visible; animation-duration: 2s; animation-name: fadeInUp;">
							<span style="background:url(https://d3l3qn7kx5ewr2.cloudfront.net/wp-content/uploads/20190409122817/c6.png) no-repeat center/contain;"></span>
						</div>
						<div class="partners-img wow fadeInUp" data-wow-duration="2s" style="visibility: visible; animation-duration: 2s; animation-name: fadeInUp;">
							<span style="background:url(https://d3l3qn7kx5ewr2.cloudfront.net/wp-content/uploads/20190409122817/c7.png) no-repeat center/contain;"></span>
						</div>
						<div class="partners-img wow fadeInUp" data-wow-duration="2s" style="visibility: visible; animation-duration: 2s; animation-name: fadeInUp;">
							<span style="background:url(https://d3l3qn7kx5ewr2.cloudfront.net/wp-content/uploads/20190409122818/c8.png) no-repeat center/contain;"></span>
						</div>
						<div class="partners-img wow fadeInUp" data-wow-duration="2s" style="visibility: visible; animation-duration: 2s; animation-name: fadeInUp;">
							<span style="background:url(https://d3l3qn7kx5ewr2.cloudfront.net/wp-content/uploads/20190409122818/c9.png) no-repeat center/contain;"></span>
						</div>
						<div class="partners-img wow fadeInUp" data-wow-duration="2s" style="visibility: visible; animation-duration: 2s; animation-name: fadeInUp;">
							<span style="background:url(https://d3l3qn7kx5ewr2.cloudfront.net/wp-content/uploads/20190409122818/c10.png) no-repeat center/contain;"></span>
						</div>
						<div class="partners-img wow fadeInUp" data-wow-duration="2s" style="visibility: visible; animation-duration: 2s; animation-name: fadeInUp;">
							<span style="background:url(https://d3l3qn7kx5ewr2.cloudfront.net/wp-content/uploads/20190409122819/c11.png) no-repeat center/contain;"></span>
						</div>
						<div class="partners-img wow fadeInUp" data-wow-duration="2s" style="visibility: visible; animation-duration: 2s; animation-name: fadeInUp;">
							<span style="background:url(https://d3l3qn7kx5ewr2.cloudfront.net/wp-content/uploads/20190409122819/c12.png) no-repeat center/contain;"></span>
						</div>
						<div class="partners-img wow fadeInUp" data-wow-duration="2s" style="visibility: visible; animation-duration: 2s; animation-name: fadeInUp;">
							<span style="background:url(https://d3l3qn7kx5ewr2.cloudfront.net/wp-content/uploads/20190409122915/part1.png) no-repeat center/contain;"></span>
						</div>
						<div class="partners-img wow fadeInUp" data-wow-duration="2s" style="visibility: visible; animation-duration: 2s; animation-name: fadeInUp;">
							<span style="background:url(https://d3l3qn7kx5ewr2.cloudfront.net/wp-content/uploads/20190409122916/part3.png) no-repeat center/contain;"></span>
						</div>
						<div class="partners-img wow fadeInUp" data-wow-duration="2s" style="visibility: visible; animation-duration: 2s; animation-name: fadeInUp;">
							<span style="background:url(https://d3l3qn7kx5ewr2.cloudfront.net/wp-content/uploads/20190409122915/part2.jpg) no-repeat center/contain;"></span>
						</div>
						<div class="partners-img wow fadeInUp" data-wow-duration="2s" style="visibility: visible; animation-duration: 2s; animation-name: fadeInUp;">
							<span style="background:url(https://d3l3qn7kx5ewr2.cloudfront.net/wp-content/uploads/20190409122917/part4.png) no-repeat center/contain;"></span>
						</div>
					</div>
				</div>
			</div>

			<footer id="colophon" class="site-footer">
				<div class="container flex">
					<div class="footer-contact wow fadeInUp" data-wow-duration="2s" style="visibility: visible; animation-duration: 2s; animation-name: fadeInUp;">
						<h2>Наші контакти</h2>
						<div class="footer-tel wow slideInLeft" data-wow-duration="1s" style="visibility: visible; animation-duration: 1s; animation-name: slideInLeft;">
							<a href="tel:0678344242">067 834 42 42</a>
							<a href="tel:0955952437">095 595 24 37</a>
							<a href="tel:0733193111">073 319 31 11</a>
						</div>
						<div class="footer-adress wow slideInLeft" data-wow-duration="1s" style="visibility: visible; animation-duration: 1s; animation-name: slideInLeft;">
							<p>м. Київ, пр. Степана Бандери 21</p>
						</div>
						<div class="footer-email wow slideInLeft" data-wow-duration="1s" style="visibility: visible; animation-duration: 1s; animation-name: slideInLeft;">
							<a href="mailto:asgard.ksm@gmail.com">asgard.ksm@gmail.com</a>
						</div>
                        <div class="footer-tel wow slideInLeft" data-wow-duration="1s" style="visibility: visible; animation-duration: 1s; animation-name: slideInLeft;">
                            <p>Графік роботи:</p>
                            <p>Пн-Пт: 9:00-18:00</p>
                            <p>Сб-Нд: в телефонному режимі</p>
                        </div>
						<form id="callrequest" method="post" class="footer-form wow slideInLeft" data-wow-duration="1s" style="visibility: visible; animation-duration: 1s; animation-name: slideInLeft;">
							<input type="text" id="u_name" name="u_name" class="modal-text" placeholder="Ім'я">
							<input type="number" id="u_phone" name="u_phone" class="modal-phone" placeholder="Телефон">
							<button type="submit" id="u_submit" class="all_btn">Надіслати</button>
							<br />
							<br />
							<span class="error_inp footer_err"></span>
						</form>
					</div>

                    <div class="site-footer--location-list">
                        <h3>Ми гарантуємо оперативну доставку по всіх районах Києва:</h3>
                        <ul class="location-list">
                            <li> Дарницький,</li>
                            <li> Дніпровський,</li>
                            <li> Печерський,</li>
                            <li> Оболонський,</li>
                            <li> Деснянський,</li>
                            <li> Голосіївський,</li>
                            <li> Святошинський,</li>
                            <li> Подільський,</li>
                            <li> Шевченківський,</li>
                            <li> Солом'янський.</li>
                        </ul>

                        <h3>а також по Київській області:</h3>
                        <ul class="location-list">
                            <li>Біла Церква,</li>
                            <li>Бориспіль,</li>
                            <li>Боярка,</li>
                            <li>Бровари,</li>
                            <li>Буча,</li>
                            <li>Васильків,</li>
                            <li>Вишневе,</li>
                            <li>Вишгород,</li>
                            <li>Іванків,</li>
                            <li>Ірпінь,</li>
                            <li>Кагарлик,</li>
                            <li>Макаров,</li>
                            <li>Миронівка,</li>
                            <li>Обухів,</li>
                            <li>Переяслав-Хмельницький,</li>
                            <li>Славутич,</li>
                            <li>Сквира,</li>
                            <li>Українка,</li>
                            <li>Фастів,</li>
                            <li>Яготин.</li>
                        </ul>
                    </div>

					<iframe class="map wow pulse" data-wow-duration="2s" src="https://www.google.com/maps/d/u/0/embed?mid=1BNivvRWmsM-BFKMt8BmTS8WD1HZqJelL&q=21A,проспект+Степана+Бандери,21А,Київ,02000" style="visibility: visible; animation-duration: 2s; animation-name: pulse;"></iframe>
<!--                    <iframe class="map wow pulse" data-wow-duration="2s" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2538.2880502307926!2d30.491369073771292!3d50.4915964896902!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x40d4cdf990428ab5%3A0x8e39a7b470ec0f66!2zMjFBLCDQv9GA0L7RgdC_0LXQutGCINCh0YLQtdC_0LDQvdCwINCR0LDQvdC00LXRgNC4LCAyMdCQLCDQmtC40ZfQsiwgMDIwMDA!5e0!3m2!1suk!2sua!4v1573640048312!5m2!1suk!2sua" style="visibility: visible; animation-duration: 2s; animation-name: pulse;"></iframe>-->
                </div>
				<br>
				<div class="container flex copy">
					<p>Всі права захищені © ТОВ КСМ Асгард</p>
				</div>
			</footer><!-- #colophon -->

			<div id="modal_wrap" class="modal_wrap">
				<div class="form_modal">
					<div id="modal_wrap_close" class="close close_modal" style="background:url(https://d3l3qn7kx5ewr2.cloudfront.net/wp-content/uploads/20190409122821/close.png) no-repeat center/contain;"></div>
					<form id="modal_request" method="post">
						<h4>Залиште свій номер телефону і ми Вам зателефонуємо.</h4>
						<input type="text" class="inp modal-text" name="m_name" id="m_name" placeholder="Ім'я">
						<br />
						<input type="tel" class="inp modal-phone" name="m_phone" id="m_phone" placeholder="Телефон">
						<br />
						<button id="send_phone" class="all_btn">Надіслати</button>
						<p id="modal_err" class="error_inp modal_err"></p>
					</form>
				</div>
			</div>
		</div>
	</div>

    <div id="request_call" type="button" class="callback-bt">
        <div class="text-call">
            <i class="fa fa-phone"></i>
            <span>Замовити<br>дзвінок</span>
        </div>
    </div>

    <div id="image-close-up" class="pop-up">
        <div class="pop-up-cont">
            <div class="close-pop-up">&times;</div>
        </div>
    </div>

	<div class="spinner" style=""><img src="https://d3l3qn7kx5ewr2.cloudfront.net/wp-content/uploads/20190409123004/spinner.svg" alt="spinner"></div>

</div><!-- #page -->

<script type="text/javascript">
    jQuery("#request_call").on("click", function(){
        jQuery("#modal_wrap").css("display", "block");
    });
    jQuery("#modal_wrap_close").on("click", function(){
        jQuery("#modal_wrap").css("display", "none");
    });
    jQuery("#send_phone").on("click", function(e){
        e.preventDefault();

        var $error = jQuery("#modal_err");
        var phone  = jQuery(this).siblings('input[name="m_phone"]').val();
        var name   = jQuery(this).siblings('input[name="m_name"]').val();

        if ( typeof phone === 'undefined' || phone.length < 10 ) {
            $error.text("Введіть існуючий номер мобільного. Зразок: 0667778855");
        } else {
            $error.text("");

            jQuery.ajax({
                url: "<?php echo admin_url( 'admin-ajax.php' ); ?>",
                type: "POST",
                data: {action: "request_call", phone: phone, name: name},
                success: function (res) {
                    var jsn = JSON.parse(res);

                    if ( typeof jsn !== "undefined" ){
                        $error.text("Виникла помилка при надсиланні повідомлення. Спробуйте пізніше.");
                    }
                    if ( jsn.success ) {
                        $error.text("Заявку надіслано! Незабаром з вами зв'яжуться.");
                        setTimeout(function () {
                            jQuery("#modal_wrap").css("display", "none");
                        }, 3000);
                    } else {
                        $error.text(jsn.message);
                    }
                }
            });

        }

    });
</script>

<?php wp_footer(); ?>

</body>
</html>
